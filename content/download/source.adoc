+++
title = "Install from Source Code"
distro = "Source Code"
summary = "Install instructions for KiCad from source code"
iconhtml = "<div><i class='fa fa-code'></i></div>"
weight = 100
+++

The most recent source code for KiCad can be downloaded from the
https://gitlab.com/kicad/code/kicad[gitlab repository].

This source contains the most recent patches and is useful for
debugging and testing. If you build KiCad from source consider
joining the https://groups.google.com/a/kicad.org/g/devlist/[developer mailing
list] to post your improvements and
https://gitlab.com/kicad/code/kicad/issues[bug-fixes to issues].

Other than the source code itself there are some other distibruted
elements that make up the full KiCad package: the KiCad
documentation (kicad-doc), the schematic, footprint, and 3D model libraries,
and the project templates.

== Stable Release

Current Version: *{{< param "release" >}}*

Tarballs intended to be used by packagers:

* link:https://gitlab.com/kicad/code/kicad/-/archive/{{< param "release" >}}/kicad-{{< param "release" >}}.tar.bz2[Source code]
* link:https://kicad-downloads.s3.cern.ch/docs/kicad-doc-{{< param "release" >}}.tar.gz[Precompiled docs], https://gitlab.com/kicad/services/kicad-doc/-/archive/{{< param "release" >}}/kicad-doc-{{< param "release" >}}.tar.bz2[doc sources]
* link:https://gitlab.com/kicad/libraries/kicad-symbols/-/archive/{{< param "release" >}}/kicad-symbols-{{< param "release" >}}.tar.bz2[symbols]
* link:https://gitlab.com/kicad/libraries/kicad-footprints/-/archive/{{< param "release" >}}/kicad-footprints-{{< param "release" >}}.tar.bz2[footprints]
* link:https://gitlab.com/kicad/libraries/kicad-packages3D/-/archive/{{< param "release" >}}/kicad-packages3D-{{< param "release" >}}.tar.bz2[packages3D]
* link:https://gitlab.com/kicad/libraries/kicad-templates/-/archive/{{< param "release" >}}/kicad-templates-{{< param "release" >}}.tar.bz2[templates]

As a packager, please work to enable all the build options, that is
all the scripting options, OCC and ngspice. With recent
versions of ngspice and wxPython Phoenix this should be easily
possible on recent distributions

Due to API instability in the Boost.Context library, KiCad 6 ships with
a replacement implementation, which is partially written in assembler
language. Because of this, several platforms may fail to compile.

== Building KiCad

Instructions can be found in compiling.md which is located in the
kicad source code under `/Documentation/development` folder.

Alternatively, you can view it online here:
link:https://dev-docs.kicad.org/en/build[Building
KiCad from Source]

