+++
title = "macOS Downloads"
distro = "macOS"
summary = "Downloads for Apple macOS 11 and above"
iconhtml = "<div><i class='fab fa-apple'></i></div>"
weight = 2
aliases = [
    "/download/osx/"
]
+++

[.initial-text]
The current stable release of KiCad is supported on macOS 11 and newer.  See
link:/help/system-requirements/[System Requirements] for more details.

Users on older versions of macOS may be able to use KiCad by installing through
link:https://ports.macports.org/port/kicad/[MacPorts] or building from source.
The KiCad team is does not have the resources to provide technical support or
bugfixes for users on these platforms.

[.initial-text]
== Stable Release

Current Version: **{{< param "release" >}}**

++++
<div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
	<div class="accordion-item">
		<div class="accordion-header" role="tab" id="mirrors-macos14-heading">
			<button role="button" class="accordion-button" data-bs-toggle="collapse" data-parent="#accordion" href="#mirrors-macos14" aria-expanded="true" aria-controls="mirrors-macos14">
				macOS 11 and newer
			</button>
		</div>
		<div id="mirrors-macos14" class="accordion-collapse collapse show" role="tabpanel" aria-labelledby="mirrors-macos14-heading">
			<div class="accordion-body">
				<div class="list-group download-list-group">
					<h4>Worldwide</h4>
					<a class="list-group-item dl-link" href="https://github.com/KiCad/kicad-source-mirror/releases/download/{{< param "release" >}}/kicad-unified-universal-{{< param "release" >}}-0.dmg">
						<img src="/img/download/github.svg" height="31" /> GitHub
					</a>
				</div>
				<div class="list-group download-list-group">
					<h4>Europe</h4>
					<a class="list-group-item dl-link" href="https://kicad-downloads.s3.cern.ch/osx/stable/kicad-unified-universal-{{< param "release" >}}-0.dmg">
						<img src="/img/about/cern-logo.png" /> CERN - Switzerland
					</a>
					<a class="list-group-item dl-link" href="https://www2.futureware.at/~nickoe/kicad-downloads-mirror/osx/stable/kicad-unified-universal-{{< param "release" >}}-0.dmg">
						Futureware - Austria
					</a>
				</div>
				<div class="list-group download-list-group">
					<h4>Asia</h4>

					<a class="list-group-item dl-link" href="https://mirrors.aliyun.com/kicad/osx/stable/kicad-unified-universal-{{< param "release" >}}-0.dmg">
						<img src="/img/download/aliyun.png" border="0" alt="AliCloud" />AlibabaCloud
					</a>
					<a class="list-group-item dl-link" href="https://mirrors.cqu.edu.cn/kicad/osx/stable/kicad-unified-universal-{{< param "release" >}}-0.dmg">
						<img src="/img/download/chongqing.jpeg" /> Chongqing University
					</a>
					<a class="list-group-item dl-link" href="https://mirror.tuna.tsinghua.edu.cn/kicad/osx/stable/kicad-unified-universal-{{< param "release" >}}-0.dmg">
						<img src="/img/download/tuna.png" />Tsinghua University
					</a>
				</div>
				<div class="list-group download-list-group">
					<h4>Australia</h4>
					<a class="list-group-item dl-link" href="https://mirror.aarnet.edu.au/pub/kicad/osx/stable/kicad-unified-universal-{{< param "release" >}}-0.dmg">
						<img src="/img/download/aarnet_logo_white.svg" height="31" > AARNet
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
++++

[.donate-hidden]
== {nbsp}
++++
	{{< getpartial "download_thanks.html" >}}
++++

== Previous Releases

Previous releases are available for download on:

https://downloads.kicad.org/kicad/macos/explore/stable


== Nightly Development Builds

The _nightly development_ builds are snapshots of the development (master branch) codebase at a specific time.
This codebase is under active development, and while we try our best, may contain more bugs than usual.
New features added to KiCad can be tested in these builds.

WARNING: Please read link:/help/nightlies-and-rcs/[Nightly Builds and Release Candidates] for
		 important information about the risks and drawbacks of using nightly builds.

KiCad nightly development builds are supported on macOS 11 and newer.
See link:/help/system-requirements/[System Requirements] for more details.

https://downloads.kicad.org/kicad/macos/explore/nightlies
